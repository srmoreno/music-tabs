// Track Player
// ----------------------------------------------------------------------------
function Player(){
    this.track = undefined;
    this.DefineCallbacks();

    this.playing = false;
    this.tic_number = 0;
    this.startup_tic = 0;
}

Player.prototype.DefineCallbacks = function(){
    this.on_load = $.Callbacks();
    this.on_stop = $.Callbacks();
    this.on_play = $.Callbacks();
    this.on_tic = $.Callbacks();
    this.on_startup_tic = $.Callbacks();
    this.on_pause = $.Callbacks();

    this.on_bar_change = $.Callbacks();
}

Player.prototype.Load = function(track){
    this.track = track;
    this.Reset();
}

Player.prototype.Reset = function() {
    this.playing = false;
    this.starting = false;
    this.startup_tic = 0;
    this.tic_number = 0;
    this.on_load.fire();
};

Player.prototype.Play = function(beats_per_minute){
    this.SetBPM(beats_per_minute);
    this.starting = true;
    this.on_play.fire();
    this.ExecuteTic()
}

Player.prototype.ExecuteTic = function(){
    var self = this;
    if (this.starting){
        this.startup_tic += 1;
        if (this.startup_tic <= this.track.beats_per_bar){
            this.on_startup_tic.fire();
            setTimeout(function(){self.ExecuteTic();}, self.tic_interval * self.track.tics_per_beat);
        }
        else{
            this.starting = false;
            this.playing = true;
        }
    }
    if (this.playing){
        this.on_tic.fire();
        setTimeout(function(){self.AdvanceTic();}, self.tic_interval);
    }}

Player.prototype.AdvanceTic = function(first_argument) {
    if (this.playing){
        var current_bar_number = this.GetBarNumber(this.tic_number);
        this.tic_number += 1;
        var next_bar_number = this.GetBarNumber(this.tic_number);
        if (next_bar_number > current_bar_number && next_bar_number > this.track.max_bar_number)
            this.Stop();
        else
            this.ExecuteTic();
    }
};

Player.prototype.Pause = function(){
    this.playing = false;
    this.starting = false;
    this.on_pause.fire();
}

Player.prototype.Stop = function(){
    this.Reset();
    this.on_stop.fire()
}

Player.prototype.GetBarNumber = function(tic_number) {
    return Math.floor(tic_number / this.track.tics_per_bar);
};

Player.prototype.SetBPM = function(beats_per_minute) {
    if (beats_per_minute === undefined)
        this.bpm = 120;
    else
        this.bpm = beats_per_minute
    this.beat_interval = Math.ceil(60 * 1000 / this.bpm);
    this.tic_interval = Math.ceil(this.beat_interval / this.track.tics_per_beat);
};
