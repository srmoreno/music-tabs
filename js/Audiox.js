// Audio X
// Multi channel audio (to play the same audio simultaneously)
// ----------------------------------------------------------------------------
function Audiox (filename, channels){
    if (channels == undefined) channels = 5;
    this.channels = channels;
    this.i = 0;
    this.audios = new Array();
    for (var i=0; i<channels; i++){
        var audio = new Audio();
        audio.src = filename;
        this.audios.push(audio);
    }
}

Audiox.prototype.play = function(){
    this.audios[this.i].play();
    this.i = (this.i+1) % this.channels;
}
