// TrackGenerator
// ----------------------------------------------------------------------------
function TrackGenerator(){

}

TrackGenerator.prototype.CreateTracks = function(){
    this.tracks = new Object();
    this.CreateBasicExercises();
    return this.tracks;
}

TrackGenerator.prototype.CreateBasicTrack = function() {
      var demo_track_channels_data = new Array();
      demo_track_channels_data.push('XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX');
      demo_track_channels_data.push('--X---X---X---X---X---X---X---X-');
      demo_track_channels_data.push('X---X---X---X---X---X---X---X---');
      demo_track_channels_data.push('--------');
      var demo_track_channels_instruments = new Array('HiHat', 'Snare','Kick','none');
      var basic_track = new Track(demo_track_channels_data, demo_track_channels_instruments, 4, 2);
      this.tracks['basic-track'] = basic_track;
};

TrackGenerator.prototype.CreateBasicExercises = function() {
      this.CreateBasicTrack();
      var instruments = new Array('HiHat', 'Snare','Kick','none');
      
      var chn = new Array();
      chn.push('XXXXXXXXXXXXXXXX XXXXXXXXXXXXXXXX XXXXXXXXXXXXXXXX XXXXXXXXXXXXXXXX XXXXXXXXXXXXXXXX XXXXXXXXXXXXXXXX XXXXXXXXXXXXXXXX XXXXXXXXXXXXXXXX XXXXXXXXXXXXXXXX XXXXXXXXXXXXXXXX XXXXXXXXXXXXXXXX XXXXXXXXXXXXXXXX XXXXXXXXXXXXXXXX XXXXXXXXXXXXXXXX XXXXXXXXXXXXXXXX XXXXXXXXXXXXXXXX '.replace(/\s/g,''));
      chn.push('--X---X---X---X- --X---X---X---X- --X---X---X---X- --X---X---X---X- --X---X---X---X- --X---X---X---X- --X---X---X---X- --X---X---X---X- --X---X---X---X- --X---X---X---X- --X---X---X---X- --X---X---X---X- --X---X---X---X- --X---X---X---X- --X---X---X---X- --X---X---X---X- '.replace(/\s/g,''));
      chn.push('X-------X------- X-------X------- -X-------X------ -X-------X------ --X-------X----- --X-------X----- ---X-------X---- ---X-------X---- ----X-------X--- ----X-------X--- -----X-------X-- -----X-------X-- ------X-------X- ------X-------X- -------X-------X -------X-------X '.replace(/\s/g,''));
      chn.push('--------');
      var track = new Track(chn, instruments, 4, 2);
      this.tracks['Sistema 1: Nota Sencilla kick-change + Feel'] = track;    

      var chn = new Array();
      chn.push('XXXXXXXXXXXXXXXX XXXXXXXXXXXXXXXX XXXXXXXXXXXXXXXX XXXXXXXXXXXXXXXX XXXXXXXXXXXXXXXX XXXXXXXXXXXXXXXX XXXXXXXXXXXXXXXX XXXXXXXXXXXXXXXX '.replace(/\s/g,''));
      chn.push('--X---X---X---X- --X---X---X---X- --X---X---X---X- --X---X---X---X- --X---X---X---X- --X---X---X---X- --X---X---X---X- --X---X---X---X- '.replace(/\s/g,''));
      chn.push('X-------X------- -X-------X------ --X-------X----- ---X-------X---- ----X-------X--- -----X-------X-- ------X-------X- -------X-------X '.replace(/\s/g,''));
      chn.push('--------');
      var track = new Track(chn, instruments, 4, 2);
      this.tracks['Sistema 1: Nota Sencilla kick-change Fast'] = track;    

      var chn = new Array();
      chn.push('XXXXXXXXXXXXXXXX XXXXXXXXXXXXXXXX XXXXXXXXXXXXXXXX XXXXXXXXXXXXXXXX XXXXXXXXXXXXXXXX XXXXXXXXXXXXXXXX XXXXXXXXXXXXXXXX XXXXXXXXXXXXXXXX XXXXXXXXXXXXXXXX XXXXXXXXXXXXXXXX XXXXXXXXXXXXXXXX XXXXXXXXXXXXXXXX XXXXXXXXXXXXXXXX XXXXXXXXXXXXXXXX XXXXXXXXXXXXXXXX XXXXXXXXXXXXXXXX '.replace(/\s/g,''));
      chn.push('--X---X---X---X- --X---X---X---X- --X---X---X---X- --X---X---X---X- --X---X---X---X- --X---X---X---X- --X---X---X---X- --X---X---X---X- --X---X---X---X- --X---X---X---X- --X---X---X---X- --X---X---X---X- --X---X---X---X- --X---X---X---X- --X---X---X---X- --X---X---X---X- '.replace(/\s/g,''));
      chn.push('XX------XX------ XX------XX------ -XX------XX----- -XX------XX----- --XX------XX---- --XX------XX---- ---XX------XX--- ---XX------XX--- ----XX------XX-- ----XX------XX-- -----XX------XX- -----XX------XX- ------XX------XX ------XX------XX X------XX------X X------XX------X '.replace(/\s/g,''));
      chn.push('--------');
      var track = new Track(chn, instruments, 4, 2);
      this.tracks['Sistema 1: 1.2 kick-change'] = track;    

      var chn = new Array();
      chn.push('XXXXXXXX XXXXXXXX XXXXXXXX XXXXXXXX XXXXXXXX XXXXXXXX XXXXXXXX XXXXXXXX XXXXXXXX XXXXXXXX XXXXXXXX XXXXXXXX XXXXXXXX XXXXXXXX XXXXXXXX XXXXXXXX XXXXXXXX XXXXXXXX XXXXXXXX XXXXXXXX '.replace(/\s/g,''));
      chn.push('--X---X- --X---X- --X---X- --X---X- --X---X- --X---X- --X---X- --X---X- --X---X- --X---X- --X---X- --X---X- --X---X- --X---X- --X---X- --X---X- --X---X- --X---X- --X---X- --X---X- '.replace(/\s/g,''));
      chn.push('XXX----- XXX----- XXX----- XXX----- XX-X---- XX-X---- XX-X---- XX-X---- X-XX---- X-XX---- X-XX---- X-XX---- XX-XX--- XX-XX--- XX-XX--- XX-XX--- X-X----- X-X----- X-X----- X-X----- '.replace(/\s/g,''));
      chn.push('--------');
      var track = new Track(chn, instruments, 4, 2);
      this.tracks['Sistema 1: Variaciones A1'] = track;    

      var chn = new Array();
      chn.push('XXXXXXXX XXXXXXXX XXXXXXXX XXXXXXXX XXXXXXXX XXXXXXXX XXXXXXXX XXXXXXXX XXXXXXXX XXXXXXXX XXXXXXXX XXXXXXXX XXXXXXXX XXXXXXXX XXXXXXXX XXXXXXXX XXXXXXXX XXXXXXXX XXXXXXXX XXXXXXXX '.replace(/\s/g,''));
      chn.push('--X---X- --X---X- --X---X- --X---X- --X---X- --X---X- --X---X- --X---X- --X---X- --X---X- --X---X- --X---X- --X---X- --X---X- --X---X- --X---X- --X---X- --X---X- --X---X- --X---X- '.replace(/\s/g,''));
      chn.push('-XXX---- -XXX---- -XXX---- -XXX---- -XX-X--- -XX-X--- -XX-X--- -XX-X--- -X-XX--- -X-XX--- -X-XX--- -X-XX--- -XX-XX-- -XX-XX-- -XX-XX-- -XX-XX-- -X-X---- -X-X---- -X-X---- -X-X---- '.replace(/\s/g,''));
      chn.push('--------');
      var track = new Track(chn, instruments, 4, 2);
      this.tracks['Sistema 1: Variaciones A2'] = track;    

      var chn = new Array();
      chn.push('XXXXXXXX XXXXXXXX XXXXXXXX XXXXXXXX XXXXXXXX XXXXXXXX XXXXXXXX XXXXXXXX XXXXXXXX XXXXXXXX XXXXXXXX XXXXXXXX XXXXXXXX XXXXXXXX XXXXXXXX XXXXXXXX XXXXXXXX XXXXXXXX XXXXXXXX XXXXXXXX '.replace(/\s/g,''));
      chn.push('--X---X- --X---X- --X---X- --X---X- --X---X- --X---X- --X---X- --X---X- --X---X- --X---X- --X---X- --X---X- --X---X- --X---X- --X---X- --X---X- --X---X- --X---X- --X---X- --X---X- '.replace(/\s/g,''));
      chn.push('--XXX--- --XXX--- --XXX--- --XXX--- --XX-X-- --XX-X-- --XX-X-- --XX-X-- --X-XX-- --X-XX-- --X-XX-- --X-XX-- --XX-XX- --XX-XX- --XX-XX- --XX-XX- --X-X--- --X-X--- --X-X--- --X-X--- '.replace(/\s/g,''));
      chn.push('--------');
      var track = new Track(chn, instruments, 4, 2);
      this.tracks['Sistema 1: Variaciones A3'] = track;    

      var chn = new Array();
      chn.push('XXXXXXXX XXXXXXXX XXXXXXXX XXXXXXXX XXXXXXXX XXXXXXXX XXXXXXXX XXXXXXXX XXXXXXXX XXXXXXXX XXXXXXXX XXXXXXXX XXXXXXXX XXXXXXXX XXXXXXXX XXXXXXXX XXXXXXXX XXXXXXXX XXXXXXXX XXXXXXXX '.replace(/\s/g,''));
      chn.push('--X---X- --X---X- --X---X- --X---X- --X---X- --X---X- --X---X- --X---X- --X---X- --X---X- --X---X- --X---X- --X---X- --X---X- --X---X- --X---X- --X---X- --X---X- --X---X- --X---X- '.replace(/\s/g,''));
      chn.push('---XXX-- ---XXX-- ---XXX-- ---XXX-- ---XX-X- ---XX-X- ---XX-X- ---XX-X- ---X-XX- ---X-XX- ---X-XX- ---X-XX- ---XX-XX ---XX-XX ---XX-XX ---XX-XX ---X-X-- ---X-X-- ---X-X-- ---X-X-- '.replace(/\s/g,''));
      chn.push('--------');
      var track = new Track(chn, instruments, 4, 2);
      this.tracks['Sistema 1: Variaciones A4'] = track;    

      var chn = new Array();
      chn.push('XXXXXXXXXXXXXXXX XXXXXXXXXXXXXXXX XXXXXXXXXXXXXXXX XXXXXXXXXXXXXXXX XXXXXXXXXXXXXXXX XXXXXXXXXXXXXXXX XXXXXXXXXXXXXXXX '.replace(/\s/g,''));
      chn.push('--X---X---X---X- --X---X---X---X- --X---X---X---X- --X---X---X---X- --X---X---X---X- --X---X---X---X- --X---X---X---X- '.replace(/\s/g,''));
      chn.push('XXX-----XXX----- XXX-----XXX----- XX-X----XX-X---- XX--X---XX--X--- XX---X--XX---X-- XX----X-XX----X- XX-----XXX-----X '.replace(/\s/g,''));
      chn.push('--------');
      var track = new Track(chn, instruments, 4, 2);
      this.tracks['Sistema 1: 1.2 kick-change'] = track;    

      var chn = new Array();
      chn.push('XXXXXXXXXXXXXXXX XXXXXXXXXXXXXXXX XXXXXXXXXXXXXXXX XXXXXXXXXXXXXXXX '.replace(/\s/g,''));
      chn.push('--X---X---X---X- --X---X---X---X- --X---X---X---X- --X---X---X---X- '.replace(/\s/g,''));
      chn.push('X---X---X---X--- -X---X---X---X-- --X---X---X---X- ---X---X---X---X '.replace(/\s/g,''));
      chn.push('--------');
      var track = new Track(chn, instruments, 4, 2);
      this.tracks['basic-1-kick-change'] = track;    

      var chn = new Array();
      chn.push('XXXXXXXXXXXXXXXX XXXXXXXXXXXXXXXX XXXXXXXXXXXXXXXX '.replace(/\s/g,''));
      chn.push('--X---X---X---X- --X---X---X---X- --X---X---X---X- '.replace(/\s/g,''));
      chn.push('X--X--X--X--X--X --X--X--X--X--X- -X--X--X--X--X-- '.replace(/\s/g,''));
      chn.push('--------');
      var track = new Track(chn, instruments, 4, 2);
      this.tracks['basic-1-2-kick-change'] = track;    

      var chn = new Array();
      chn.push('XXXXXXXXXXXXXXXX XXXXXXXXXXXXXXXX XXXXXXXXXXXXXXXX XXXXXXXXXXXXXXXX XXXXXXXXXXXXXXXX'.replace(/\s/g,''));
      chn.push('--X---X---X---X- --X---X---X---X- --X---X---X---X- --X---X---X---X- --X---X---X---X-'.replace(/\s/g,''));
      chn.push('X-X-X-X-X-X-X-X- -X-X-X-X-X-X-X-X X--X--X--X--X--X --X--X--X--X--X- X---X---X---X---'.replace(/\s/g,''));
      chn.push('--------');
      var track = new Track(chn, instruments, 4, 2);
      this.tracks['basic-1-1-kick-change'] = track;    

      var chn = new Array();
      chn.push('X-X-X-X-X-X-X- X-X-X-X-X-X-X- X-X-X-X-X-X-X- X-X-X-X-X-X-X- X-X-X-X-X-X-X- X-X-X-X-X-X-X-'.replace(/\s/g,''));
      chn.push('----X----X---X ----X---XXXXXX ----X----X---X ----X-XXXXXXXX ----XX-X---X-- ----XX-X---XXX'.replace(/\s/g,''));
      chn.push('X-X----X---X-- X-X----X---X-- X-X----X---X-- X-X----X---X-- X-X---X--X---X X-X---X--X---X'.replace(/\s/g,''));
      chn.push('--------');
      var track = new Track(chn, instruments, 7, 2);
      this.tracks['base-7-2'] = track;    

};

