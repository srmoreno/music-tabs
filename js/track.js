// Track
// ----------------------------------------------------------------------------
function Track(channels, instruments, beats_per_bar, tics_per_beat){
    this.channels = channels;
    this.instruments = instruments;
    this.beats_per_bar = beats_per_bar;
    this.tics_per_beat = tics_per_beat;
    this.tics_per_bar = beats_per_bar * tics_per_beat;
    this.max_bar_number = this.DefineMaxBarNumber();
    this.max_tic_number = (this.max_bar_number+1) * this.tics_per_bar - 1;
    this.CompleteChannelsData();
}

Track.prototype.GetBar = function(bar_number){
    var bar = new Array();
    var init_tic = bar_number * this.tics_per_bar;
    var end_tic = (1 + bar_number) * this.tics_per_bar;
    for (var i=0; i < this.channels.length; i++){
        bar.push(this.channels[i].slice(init_tic, end_tic))
    }
    return bar;
}

Track.prototype.DefineMaxBarNumber = function(bar_number){
    var max_tic = 0;
    for (var i=0; i < this.channels.length; i++){
        max_tic = Math.max(max_tic, this.channels[i].length);
    }
    if (max_tic%this.tics_per_bar == 0)
        return max_tic/this.tics_per_bar - 1
    else
        return Math.floor(max_tic/this.tics_per_bar);
 }

Track.prototype.CompleteChannelsData = function(data) {
    for (var i=0; i < this.channels.length; i++){
        var missing = this.max_tic_number + 1 - this.channels[i].length ;
        if (missing > 0)
            this.channels[i] = this.channels[i] + new Array(missing + 1).join('-');
    }
};
 