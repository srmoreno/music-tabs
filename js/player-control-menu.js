// http://www.html5rocks.com/en/tutorials/file/dndfiles/
// Track Player Control Menu
// ----------------------------------------------------------------------------
function PlayerControlMenu(holder_name, player, tracklist){
    this.holder = $('#'+holder_name);
    this.player = player;
    this.tracklist = tracklist;
    this.CreateUI();
    this.LoadTrackList();
    this.SubscribeToPlayerCallbacks();
}

PlayerControlMenu.prototype.CreateUI = function(){
    this.ui = new Array();
    this.ui['play'] = $('<input type="button" value="Play" id="play_button" disabled="disabled"/>');
    this.ui['pause'] = $('<input type="button" value="Pause" id="pause_button" style="display: none," disabled="disabled"/>');
    this.ui['stop'] = $('<input type="button" value="Stop" id="stop_button" disabled="disabled"/>');
    this.ui['bpm'] = $('<input type="number" value="60"/>');
    this.ui['load'] = $('<input type="button" value="Load" id="load_button"/>');
    this.ui['track'] = $('<select id="track"/>');

    var self = this;
    this.holder.append(this.ui['play']);
    this.ui['play'].click(function (){self.Play();});
    this.holder.append(this.ui['pause']);
    this.ui['pause'].click(function (){self.Pause();})
    this.holder.append(this.ui['stop']);
    this.ui['stop'].click(function (){self.Stop();})
    this.holder.append(this.ui['bpm']);
    this.ui['bpm'].click(function (){self.SetBPM();})
    this.holder.append(this.ui['track']);
    this.holder.append(this.ui['load']);
    this.ui['load'].click(function (){self.LoadTrack();})
}

PlayerControlMenu.prototype.SubscribeToPlayerCallbacks = function() {
    var self = this;
    this.player.on_load.add(function(){self.SetUIReady();});
    this.player.on_stop.add(function(){self.SetUIReady();});
    this.player.on_pause.add(function(){self.SetUIPaused();});
    this.player.on_play.add(function(){self.SetUIPlaying();});
};

PlayerControlMenu.prototype.SetUIReady = function() {
    this.ui['stop'].prop('disabled', true);
    this.ui['play'].prop('disabled', false);
};
PlayerControlMenu.prototype.SetUIPaused = function() {
    this.ui['pause'].prop('disabled', true);
    this.ui['play'].prop('disabled', false);
};
PlayerControlMenu.prototype.SetUIPlaying = function() {
    this.ui['play'].prop('disabled', true);
    this.ui['stop'].prop('disabled', false);
    this.ui['pause'].prop('disabled', false);
};

PlayerControlMenu.prototype.LoadTrackList = function() {
    for (var key in this.tracklist){
        if (this.tracklist.hasOwnProperty(key)){
            this.ui['track'].append($('<option value="'+ key +'">'+ key.replace('-',' ') +'</>'));
        }
    }
};

PlayerControlMenu.prototype.Play = function(e){
    var bpm = this.ui['bpm'].val();
    this.player.Play(bpm);
}

PlayerControlMenu.prototype.Pause = function(e){
    this.player.Pause();
}

PlayerControlMenu.prototype.Stop = function(){
    this.player.Stop();
}

PlayerControlMenu.prototype.LoadTrack = function(){
    var track_name = this.ui['track'].val();
    this.player.Load(this.tracklist[track_name]);
    this.SetBPM();
}

PlayerControlMenu.prototype.SetBPM = function() {
    var bpm = parseInt(this.ui['bpm'].val());
    if (bpm < 0)
        bpm = 1;
    if (bpm > 240)
        bpm = 240;
    this.player.SetBPM(bpm);
    this.ui['bpm'].val(bpm);
};
