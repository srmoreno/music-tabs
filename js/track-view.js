// Track View
// ----------------------------------------------------------------------------
function TrackView(id){
    this.tic_width = 400;
    this.channel_height = 20;
    this.tics_to_show = 20;
    this.bars_per_line = 2;
    this.colors = ['yellow','red','blue','green','purple']

    this.display_tic_numbers = true&&false;
    this.display_beat_numbers = true;
    this.display_bar_numbers = true&&false;
    this.table = $('<div class="track"></div>');
}

TrackView.prototype.GetElement = function() {
    return this.table;
};

TrackView.prototype.LoadTrack = function(track) {
    this.track = track;
    this.table.empty();
    this.DisplayTrack();
    this.LoadSounds();
}

TrackView.prototype.LoadSounds = function() {
    this.sounds = new Array();
    for (var i=0; i < this.track.channels.length; i++){
        var instrument = this.track.instruments[i];
        var audio;
        if (instrument != 'none'){
            audio = new Audiox('audio/'+ instrument +'.mp3');
        }
        else{
            audio = new Object();
            audio.play = new function(){};
        }
        this.sounds.push(audio);
    }
};

TrackView.prototype.DisplayTrack = function() {
    var i;
    var line;
    for (i=0; i<=this.track.max_bar_number; i++){
        if (i%this.bars_per_line ==0)
            var line = $('<div class="track-line" id="line_'+i/this.bars_per_line+'"></div>');
        var base_tic = i*this.track.tics_per_bar;
        for (var j=0; j<this.track.tics_per_bar; j++){
            this.DisplayTic(base_tic + j, line);
        }
        if (i%this.bars_per_line == this.bars_per_line - 1)
            this.table.append(line);
    }
    if (i%this.bars_per_line < this.bars_per_line)
        this.table.append(line);
};

TrackView.prototype.DisplayTic = function(tic_number, holder) {
    var tic_holder = $('<div tic_number='+tic_number+' class="tic"></div>')
    if (this.display_bar_numbers)
        this.DisplayBarNumber(tic_number, tic_holder);
    if (this.display_beat_numbers)
        this.DisplayBeatNumber(tic_number, tic_holder);
    if (this.display_tic_numbers)
        this.DisplayTicNumber(tic_number, tic_holder);
    for (var i=0; i < this.track.channels.length; i++){
        this.DisplayChannelData(i, tic_number, this.track.channels[i], tic_holder)
    }
    holder.append(tic_holder);
};

TrackView.prototype.DisplayBarNumber = function(current_tic, holder) {
    var tic = $('<div class="tic-data barN"></div>');
    if (current_tic%this.track.tics_per_bar == 0)
        tic.text(String(current_tic/this.track.tics_per_bar +1));
    holder.append(tic);
};

TrackView.prototype.DisplayBeatNumber = function(current_tic, holder) {
    var tic = $('<div class="tic-data beatN"></div>');
    if (current_tic%this.track.tics_per_beat == 0)
        tic.text(String((current_tic/this.track.tics_per_beat) % this.track.beats_per_bar + 1));
    holder.append(tic);
};

TrackView.prototype.DisplayTicNumber = function(current_tic, holder) {
    var tic = $('<div class="tic-data ticN">'+String(current_tic + 1)+'</div>');
    holder.append(tic);
};

TrackView.prototype.DisplayChannelData = function(channel, current_tic, data, holder) {
    var note = $('<div class="tic-data">'+ data[current_tic] +'</div>');
    holder.append(note);
};

TrackView.prototype.PlayTic = function(tic_number, tic_interval) {
    var new_tic = tic_number + this.tics_to_show - this.tics_to_left;
    if (new_tic >= this.tics_to_show && new_tic <= this.track.max_tic_number){
        this.DisplayTic(new_tic, tic_interval)
    }
    this.ScrollToTicLine(tic_number);
    this.AnimateTic(tic_number, tic_interval);
    this.PlayTicSounds(tic_number);
};

TrackView.prototype.AnimateTic = function(tic_number, tic_interval) {
    var element = $('[tic_number='+  tic_number +']')
    element.css('background-color','lightgrey');
    setTimeout(function(){element.css('background-color','white');}, tic_interval);
    var num_channels = this.track.channels.length;
    var notes = element.children().slice(-num_channels)
    for (var i=0; i < num_channels; i++){
        var note = this.track.channels[i][tic_number];
        if (note !='-'){
            $(notes[i]).css('background-color', this.colors[i])
        }
    }
    setTimeout(function(){notes.css('background-color','white');}, tic_interval);
};

TrackView.prototype.PlayTicSounds = function(tic_number) {
    for (var i=0; i < this.track.channels.length; i++){
        var note = this.track.channels[i][tic_number];
        if (note !='-')
            this.sounds[i].play();
    }
};

TrackView.prototype.ScrollToTicLine = function(tic_number) {
    if (tic_number%(this.bars_per_line*this.track.tics_per_bar) == 0){
    var line_num = tic_number / (this.bars_per_line*this.track.tics_per_bar);
    //$('body,html').prop('scrollTop', $('#line_'+ line_num).position().top);
    $('body,html').animate({scrollTop: $('#line_'+ line_num).position().top}, 400);
}

};