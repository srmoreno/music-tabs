// Player View
// ----------------------------------------------------------------------------
function PlayerView(holder_id, player){
    this.holder = $('#'+holder_id);
    this.player = player;
    this.track_view = new TrackView('1');
    this.holder.append(this.track_view.GetElement());
    this.SubscribeToPlayerCallbacks();
    
    this.beat_sound = new Audiox('audio/Tic.mp3');
}

PlayerView.prototype.SubscribeToPlayerCallbacks = function() {
    var self = this;
    this.player.on_load.add(function(){self.LoadTrack();});
    this.player.on_tic.add(function(){self.PlayTic();});
    this.player.on_startup_tic.add(function(){self.PlayStartupTic();});
    this.player.on_stop.add(function(){$('body,html').animate({scrollTop: 0}, 400);});
};

PlayerView.prototype.LoadTrack = function() {
    this.track_view.LoadTrack(this.player.track);
};

PlayerView.prototype.PlayTic = function() {
    this.track_view.PlayTic(this.player.tic_number, this.player.tic_interval);
};

PlayerView.prototype.PlayStartupTic = function() {
    this.beat_sound.play()
};
